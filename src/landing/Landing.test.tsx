import enzyme, { shallow } from 'enzyme';
import React from 'react';
import { unmountComponentAtNode } from 'react-dom';
import { ILandingProps, ILandingState, Landing } from './Landing';
import Adapter from 'enzyme-adapter-react-16';

enzyme.configure({ adapter: new Adapter() });

function flushPromises() {
	return new Promise((resolve) => setImmediate(resolve));
}

let container: any = null;
beforeEach(() => {
	container = document.createElement('div');
	document.body.appendChild(container);
});

afterEach(() => {
	unmountComponentAtNode(container);
	container.remove();
	container = null;
});

describe('Landing Component', () => {
	it('renders successfully', async () => {
		const landing = shallow<React.Component<ILandingProps, ILandingState>>(<Landing />);

		expect(landing.instance().state.loading).toEqual(true);
		await flushPromises();
		setTimeout(() => {
			expect(landing.instance().state.loading).toEqual(false);
		}, 5000);
	});
});
