import { Col, Layout, Row, Select } from 'antd';
import React, { Component } from 'react';
import { Book } from '../book/Book';
import { IBook } from '../book/Book.interface';
import { booksAPI } from '../__common/Books.api';
import './Landing.scss';
import Dayjs from 'dayjs';

const { Header, Content } = Layout;
const { Option } = Select;

export interface ILandingProps {}

export interface ILandingState {
	loading: boolean;
	orderBy: ORDER_BY_OPTIONS;
	books: IBook[];
}

export enum ORDER_BY_OPTIONS {
	PUBLISH_DATE = 'publish_date',
	BOOK_TITLE = 'book_title',
}

export class Landing extends Component<ILandingProps, ILandingState> {
	get orderedBooks() {
		return this.state.books.sort((bookA, bookB) => {
			let result = 0;
			switch (this.state.orderBy) {
				case ORDER_BY_OPTIONS.PUBLISH_DATE:
					result = bookA.publishDate.isAfter(bookB.publishDate) ? 1 : -1;
					break;
				case ORDER_BY_OPTIONS.BOOK_TITLE:
				default:
					result = bookA.title > bookB.title ? 1 : -1;
					break;
			}

			return result;
		});
	}

	constructor(props: ILandingProps) {
		super(props);

		this.state = {
			loading: false,
			orderBy: ORDER_BY_OPTIONS.BOOK_TITLE,
			books: [],
		};
	}

	async fetchBooks() {
		this.setState({ loading: true });

		const books = await booksAPI.fetchBooks();

		this.setState({
			loading: false,
			books: books,
		});
	}

	componentDidMount() {
		this.fetchBooks();
	}

	async deleteBook(id: string) {
		await booksAPI.deleteBook(id);

		this.setState({
			books: this.state.books.filter((book) => book.id !== id),
		});
	}

	onOrderByChange(value: ORDER_BY_OPTIONS) {
		this.setState({
			orderBy: value,
		});
	}

	render() {
		return (
			<Layout className="books-container">
				<Header className="books-header">
					<Select
						className="books-order-by"
						defaultValue={this.state.orderBy}
						size="large"
						loading={this.state.loading}
						onChange={this.onOrderByChange.bind(this)}
					>
						<Option value={ORDER_BY_OPTIONS.PUBLISH_DATE}>Publish Date</Option>
						<Option value={ORDER_BY_OPTIONS.BOOK_TITLE}>Book Title</Option>
					</Select>
				</Header>
				<Content className="book-list">
					<Row justify="start">
						{this.state.loading
							? new Array(18).fill(null).map((item, index) => (
									<Col span={24} md={12} lg={8} key={index}>
										<Book
											loading
											book={{ id: '', description: '', title: '', publishDate: Dayjs() }}
											onDelete={this.deleteBook.bind(this)}
										/>
									</Col>
							  ))
							: this.orderedBooks.map((book) => (
									<Col span={24} md={12} lg={8} key={book.id}>
										<Book loading={false} book={book} onDelete={this.deleteBook.bind(this)} />
									</Col>
							  ))}
					</Row>
				</Content>
			</Layout>
		);
	}
}
