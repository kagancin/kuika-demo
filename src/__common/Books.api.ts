import { IBook } from '../book/Book.interface';
import Dayjs from 'dayjs';

class BooksAPI {
	private _apiFetchBooks = 'http://fakerestapi.azurewebsites.net/api/Books';

	async fetchBooks(): Promise<IBook[]> {
		const response = await fetch(this._apiFetchBooks);
		const rawBooks = await response.json();
		return rawBooks.map((rawBook: any) => ({
			id: rawBook.ID,
			description: rawBook.Description,
			publishDate: Dayjs(rawBook.PublishDate),
			title: rawBook.Title,
		}));
	}

	async deleteBook(id: string) {
		return new Promise((resolve) => {
			setTimeout(() => {
				resolve(true);
			}, 250);
		});
	}
}

export const booksAPI = new BooksAPI();
