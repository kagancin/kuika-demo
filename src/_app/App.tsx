import React from 'react';
import './App.scss';
import { Landing } from '../landing/Landing';

function App() {
	return (
		<section id="root">
			<Landing />
		</section>
	);
}

export default App;
