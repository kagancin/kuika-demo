import React from 'react';
import { render, act } from '@testing-library/react';
import App from './App';
import { unmountComponentAtNode } from 'react-dom';

Object.defineProperty(window, 'matchMedia', {
	writable: true,
	value: jest.fn().mockImplementation((query) => ({
		matches: false,
		media: query,
		onchange: null,
		addListener: jest.fn(), // deprecated
		removeListener: jest.fn(), // deprecated
		addEventListener: jest.fn(),
		removeEventListener: jest.fn(),
		dispatchEvent: jest.fn(),
	})),
});

it('renders without crashing', () => {
	const div: any = document.createElement('div');
	render(<App />, div);
	unmountComponentAtNode(div);
});
