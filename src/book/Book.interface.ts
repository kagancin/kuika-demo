import { Dayjs } from 'dayjs';

export interface IBook {
	id: string;
	description: string;
	publishDate: Dayjs;
	title: string;
}
