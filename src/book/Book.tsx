import React, { Component } from 'react';
import { IBook } from './Book.interface';
import './Book.scss';
import { Card, Button } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';

export interface IBookProps {
	book: IBook;
	loading: boolean;
	onDelete: (id: string) => void;
}

export interface IBookState {
	deleting: boolean;
}

export class Book extends Component<IBookProps, IBookState> {
	constructor(props: IBookProps) {
		super(props);

		this.state = {
			deleting: false,
		};
	}

	deleteBook() {
		this.setState({
			deleting: true,
		});

		this.props.onDelete(this.props.book.id);
	}

	render() {
		return (
			<Card
				loading={this.props.loading}
				title={this.props.book.title}
				extra={<span className="book-publish-date">{this.props.loading ? '' : this.props.book.publishDate.format('DD MMMM YYYY')}</span>}
				className="book-card"
			>
				<div className="book-card__content">
					<p>{this.props.book.description}</p>

					<Button loading={this.state.deleting} className="book-card-delete-action" type="text" danger onClick={this.deleteBook.bind(this)}>
						{this.state.deleting ? <span></span> : <DeleteOutlined />}
					</Button>
				</div>
			</Card>
		);
	}
}
