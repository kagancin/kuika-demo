import { act } from 'react-dom/test-utils';
import { render as basicRender } from '@testing-library/react';
import { render } from 'react-dom';
import Dayjs from 'dayjs';
import React from 'react';
import { unmountComponentAtNode } from 'react-dom';
import { Book } from './Book';
import { IBook } from './Book.interface';

const now = Dayjs();
const testBook: IBook = {
	id: '1',
	title: 'My Test Book',
	description: 'My Test Description',
	publishDate: now,
};

const mockCallback = jest.fn(() => {});

let container: any = null;
beforeEach(() => {
	container = document.createElement('div');
	document.body.appendChild(container);
});

afterEach(() => {
	unmountComponentAtNode(container);
	container.remove();
	container = null;
});

describe('Book Component', () => {
	it('renders correct information', () => {
		const { getByText } = basicRender(<Book book={testBook} loading={false} onDelete={mockCallback} />);
		const bookTitleTest = getByText(/my test book/i);
		const bookDescriptionTest = getByText(/my test description/i);
		const bookPublishDateTest = getByText(now.format('DD MMMM YYYY'));
		expect(bookTitleTest).toBeInTheDocument();
		expect(bookDescriptionTest).toBeInTheDocument();
		expect(bookPublishDateTest).toBeInTheDocument();
	});

	it('calls onDelete callback', () => {
		act(() => {
			render(<Book book={testBook} loading={false} onDelete={mockCallback} />, container);
			const button = container.querySelector('Button');
			button.dispatchEvent(new MouseEvent('click', { bubbles: true }));
		});

		expect(mockCallback).toBeCalledWith('1');
	});
});
